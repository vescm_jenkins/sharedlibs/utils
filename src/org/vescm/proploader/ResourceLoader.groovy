package org.vescm.proploader

import java.util.Properties
import org.json.JSONObject
import java.io.StringReader

class ResourceLoader implements Serializable {
    private final def steps

    public ResourceLoader(def steps) {
        assert steps != null : 'Construtor de ' +
            getClass().toString() +
            'nao deve ser null'
        this.steps = steps
    }

    public Properties loadProperties(String resourceName) {
        String content  = steps.libraryResource(resourceName)

        Properties props = new Properties()
        props.load(new StringReader(content))

        return props
    }

    public JSONObject loadJson(String resourceName) {
        String jsonText = steps.libraryResource(resourceName)

        JSONObject json = new JSONObject(jsonText)

        return json
    }
}

