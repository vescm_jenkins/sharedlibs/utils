def call(List<Map> userPassList, Closure closure) {
    node {
        userPassList.each { m ->
            assert m['var'] != null
            assert m['password'] != null
        }

        withMasked(userPassList) {
            closure()
        }
    }
}

def withMasked(List<Map> map, Closure closure) {
    wrap([$class: 'MaskPasswordsBuildWrapper', varPasswordPairs: map]) {
        withEnv(map.collect { "${it.var}=${it.password}" }) {
            closure()
        }
    }
}

// vim: et sw=4 ts=4 sts=4 ft=groovy :
