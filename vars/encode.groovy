import java.net.URLEncoder

def call(String text) {
    assert text != null
    return URLEncoder.encode(text, "UTF-8")
}

// vim: et sw=4 ts=4 sts=4 ft=groovy :
