import org.vescm.proploader.ResourceLoader

def call(String resource) {
  ResourceLoader resLoader = new ResourceLoader(this)
  return resLoader.loadJson(resource)
}

// vim: et sw=4 ts=4 sts=4 ft=groovy :
