# Jenkins Shared Libraries

Example made in jenkins declarative pipeline 2.263.1:

``` groovy
@Library("Utils")
import org.vescm.proploader.ResourceLoader

pipeline {
    agent any

    stages {
        stage('Tests') {
            steps {
                script {
                    def listmap = []
                    listmap.add([var: 'username', password: 'vescm'])
                    listmap.add([var: 'password', password: '#my%Password$'])

                    hiddenOutput(listmap) {
                         listmap.each { s -> println(s) }
                    }

                    println('plain: ' + listmap[1]['password'])

                    println('UTF-8 encoded: ' + encode(listmap[1]['password']))

                    loadProperties('hosts.properties')
                    loadJson('hosts.json')
                    loadJson('tests/hello.json')

                    // alternative usage:
                    //     ResourceLoader e = new ResourceLoader(this)
                    //     e.loadJson('hosts.json')
                    //     e.loadProperties('hosts.json')
                }
            }
        }
    }
}
```

